package com.example.simplerapidreact2022;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import com.opencsv.CSVWriter;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MainActivity extends AppCompatActivity  {
    final String[] tournamentLevels =  new String[] {"None", "Practice", "Qualification", "Playoff"};
    final String[] climberOptions =  new String[] {"None", "Low", "Mid", "High", "Traversal"};
    final String[] stationOptions = new String[] {"Red 1", "Red 2", "Red 3", "Blue 1", "Blue 2", "Blue 3"};

    File PATH;
    JSONArray stationsList;
    MatchData matchData;

    //scoutNumber is 0-5, tabletNumber is 0-11
    int scoutNumber = 0;
    int tabletNumber = 0;

    //adapters
    ArrayAdapter<CharSequence> climberAdapter;
    ArrayAdapter<CharSequence> tournamentLevelAdapter;
    ArrayAdapter<CharSequence> stationsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PATH = getApplicationContext().getExternalFilesDir(null);

        //submit button logic
        findViewById(R.id.submit).setOnClickListener(view -> submit());

        //finds scout number (station) and tablet number
        String tabletName;
        try {
            tabletName = Settings.Secure.getString(getContentResolver(), "bluetooth_name");
        } catch (NullPointerException e) {
            tabletName = "Scout 1";
            e.printStackTrace();
        }
        if (tabletName == null) tabletName = "Scout 1";
        Log.i("tabletName", tabletName);
        Log.i("scoutNumber", tabletName.split(" ")[1]);
        tabletNumber = Integer.parseInt(tabletName.split(" ")[1])-1;
        scoutNumber = tabletNumber % 6;

        //initialize matchData object with name, match, and team EditTexts, then all other fields
        this.matchData = new MatchData(findViewById(R.id.nameTextView), findViewById(R.id.matchNumber), findViewById(R.id.teamNumber));

        this.matchData.addField("tournamentLevel", findViewById(R.id.tournamentLevelSpinner), "Qualification", null, "String");
        this.matchData.addField("station", findViewById(R.id.stationSpinner), stationOptions[scoutNumber], null, "String");
        this.matchData.addField("autoHigh", findViewById(R.id.autoHighTextView), 0, 0, "int");
        this.matchData.addField("autoLow", findViewById(R.id.autoLowTextView), 0, 0, "int");
        this.matchData.addField("autoMissed", findViewById(R.id.autoMissedTextView), 0, 0, "int");
        this.matchData.addField("taxi", findViewById(R.id.autoTaxiCheckBox), false, false, "boolean");
        this.matchData.addField("human", findViewById(R.id.humanCheckBox), false, false, "boolean");
        this.matchData.addField("teleHigh", findViewById(R.id.teleHighTextView), 0, 0, "int");
        this.matchData.addField("teleLow", findViewById(R.id.teleLowTextView), 0, 0, "int");
        this.matchData.addField("teleMissed", findViewById(R.id.teleMissedTextView), 0, 0, "int");
        this.matchData.addField("climb", findViewById(R.id.climberSpinner), "None", "None", "string");
        this.matchData.addField("broke", findViewById(R.id.brokeCheckBox), false, false, "boolean");
        this.matchData.addField("comments", findViewById(R.id.comments), "", "", "string");

        
        climberAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, climberOptions);
        climberAdapter.setDropDownViewResource(R.layout.spinner_item);
        ((AutoCompleteTextView) matchData.getField("climb").getInput()).setAdapter(climberAdapter);

        tournamentLevelAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, tournamentLevels);
        tournamentLevelAdapter.setDropDownViewResource(R.layout.spinner_item);
        ((AutoCompleteTextView) matchData.getField("tournamentLevel").getInput()).setAdapter(tournamentLevelAdapter);
        ((AutoCompleteTextView) matchData.getField("tournamentLevel").getInput()).setOnItemClickListener((parent, view, pos, l) -> {
            matchData.setField("tournamentLevel", parent.getItemAtPosition(pos).toString());
            updateTournamentLevel();
        });

        stationsAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, stationOptions);
        stationsAdapter.setDropDownViewResource(R.layout.spinner_item);
        ((AutoCompleteTextView) matchData.getField("station").getInput()).setAdapter(stationsAdapter);
        ((AutoCompleteTextView) matchData.getField("station").getInput()).setOnItemClickListener((parent, view, pos, l) -> matchData.setField("station", stationOptions[pos]));
        
        this.matchData.getField("team").getInput().addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String team = matchData.getField("team").updateValue();
                if (team.equals("2996")) {
                    findViewById(R.id.autoMissedLayout).setVisibility(View.VISIBLE);
                    findViewById(R.id.autoMissedHeaderTextView).setVisibility(View.VISIBLE);

                    findViewById(R.id.teleMissedLayout).setVisibility(View.VISIBLE);
                    findViewById(R.id.teleMissedHeaderTextView).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.autoMissedLayout).setVisibility(View.GONE);
                    findViewById(R.id.autoMissedHeaderTextView).setVisibility(View.GONE);

                    findViewById(R.id.teleMissedLayout).setVisibility(View.GONE);
                    findViewById(R.id.teleMissedHeaderTextView).setVisibility(View.GONE);
                }

            }

            @Override public void afterTextChanged(Editable editable) { }
        });

        updateTournamentLevel();
    }

    public void updateTournamentLevel() {
        final File stationsFile = new File(PATH, "stations.json");

        TextWatcher matchNumberTextListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length() != 0) {
                    int match = Integer.parseInt(charSequence.toString())-1;
                    if (match > -1) {
                        if (match >= stationsList.size()) matchData.getField("match").setValue("");
                        else matchData.getField("team").setValue(stationsList.get(match));
                    }
                } else matchData.getField("team").setValue("");
            }
            @Override
            public void afterTextChanged(Editable editable) { }
        };
        Log.i("tournamentLevel", matchData.getFieldValue("tournamentLevel"));
        Log.i("stationsFile", String.valueOf(stationsFile.exists()));

        if (matchData.getFieldValue("tournamentLevel").equals("Qualification")) {
            if (stationsFile.exists()) {
                JSONParser parser = new JSONParser();

                try {
                    stationsList = (JSONArray) parser.parse(new FileReader(stationsFile));
                    stationsList = (JSONArray) stationsList.get(scoutNumber);
                    Log.i("stations", stationsList.toString());
                } catch (IOException | ParseException e) {
                    e.printStackTrace();
                }

                matchData.getField("match").getInput().addTextChangedListener(matchNumberTextListener);
                matchData.getField("station").setValue(stationOptions[scoutNumber]);
                matchData.getField("station").getInput().setEnabled(false);
            } else {
                Toast.makeText(getBaseContext(), "No Schedule was found.",
                        Toast.LENGTH_LONG).show();
            }
        } else {
            matchData.getField("match").getInput().removeTextChangedListener(matchNumberTextListener);
            matchData.getField("station").getInput().setEnabled(true);
        }

    }

    public void updateData(@NonNull View v) {
        String tag = v.getTag().toString();

        try {
            MatchData.InputField field = matchData.getField(tag.split(":")[0]);
            if (tag.contains("Plus")) field.increment();
            else field.decrement();
        } catch (Exception e) {
            Log.e("increment()", "COULD NOT FIND OPERATION FOR TAG '" + tag + "'");
            Log.e("error", e.toString());
        }
    }

    public void submit() {
        Log.i("valid", String.valueOf(matchData.valid()));
        matchData.updateValue();
        if (matchData.valid()) submitDialog();
        else Toast.makeText(getBaseContext(),"Make sure all required fields are filled out",Toast.LENGTH_SHORT).show();
    }

    public void submitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Confirm Submit")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> {
                    Toast.makeText(getBaseContext(), "Submitting...",
                            Toast.LENGTH_SHORT).show();
                    try {
                        if (matchData.getField("match").getValue().equals("2996")) {
                            writeData("matches2996.csv");
                        }
                        writeData("matches"+(tabletNumber+1)+".csv");
                        matchData.reset();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                })
                .setNegativeButton("No", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void writeData(String fileName) throws IOException {

        CSVWriter writer;
        final File file = new File(PATH, fileName);

        //If the file exists, it appends, if it is empty it creates the file and adds the header row
        if (file.exists() && !file.isDirectory()) {
            writer = new CSVWriter(new FileWriter(file.getPath(), true));
        } else {
            writer = new CSVWriter(new FileWriter(file.getPath()));
            writer.writeNext(this.matchData.fields.keySet().toArray(new String[0]));
        }

        //adds match data to csv
        Log.i("match data", Arrays.toString(matchData.getArray()));
        writer.writeNext(matchData.getArray());
        writer.close();
    }

}
package com.example.simplerapidreact2022;

import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class MatchData {

    public Map<String, InputField> fields;
    private Map<String, Object> resets;


    public MatchData(TextInputEditText editName, TextInputEditText editMatch, TextInputEditText editTeam) {
        fields = new LinkedHashMap<>();
        resets = new HashMap<>();
        this.addField("timestamp", null, String.valueOf(new Timestamp(System.currentTimeMillis())), null, "string");
        this.addField("name", editName, "Judd", null, "string");
        this.addField("match", editMatch, editMatch.getText().toString(), "", "int");
        this.addField("team", editTeam, editTeam.getText().toString(), "", "int");
    }

    public void updateView() {
        fields.forEach((key, field) -> field.updateValue());
    }

    public void updateValue() {
        fields.forEach((key, field) -> field.updateValue());
    }

    public void addField(String name, TextView editField, Object initValue, Object reset, String type) {
        fields.put(name, new InputField<>(editField, initValue, type));
        resets.put(name, reset);
    }

    public InputField getField(String key) {
        return Objects.requireNonNull(fields.get(key));
    }

    public String getFieldValue(String key) {
        return Objects.requireNonNull(fields.get(key)).getValue();
    }

    public void setField(String key, String value) {
        Objects.requireNonNull(fields.get(key)).setValue(value);
    }

    public String[] getArray() {
        return fields.values().stream().map(InputField::getValue).toArray(String[]::new);
    }

    public void reset() {
        fields.forEach((key, field) -> {
            if (key.equals("match")) {
                field.increment();
            } else if (resets.get(key) != null) {
                field.setValue(resets.get(key));
            }
        });

        updateView();
    }


    public boolean valid() {
        return fields.get("name").exists() && fields.get("match").exists() && fields.get("team").exists()
                && fields.get("tournamentLevel").exists() && fields.get("station").exists();
    }


    class InputField<S extends TextView> {

        private final S input;
        private Object value;
        private final String type;

        public InputField(S inputObj, Object value, String type) {
            this.input = inputObj;
            this.type = type;
            this.setValue(value);
        }

        public S getInput() {
            return input;
        }

        public String getValue() {
            return value.toString();
        }

        public <T> void setValue(Object v) {
            this._setValue(v);
            if (input instanceof AutoCompleteTextView) {
                AutoCompleteTextView spinner = (AutoCompleteTextView) input;
                ArrayAdapter<T> adapter = (ArrayAdapter<T>) spinner.getAdapter();
                spinner.setAdapter(null);
                spinner.setText(this.getValue());
                spinner.setAdapter(adapter);
            } else if (input instanceof CheckBox) {
                ((CheckBox) input).setChecked((Boolean) this.value);
            } else this.update();
        }

        private void _setValue(Object v) {
            if (type.equals("int") && v instanceof String) {
                if (!v.equals("")) value = Integer.valueOf((String) v);
                else value = v;
            } else if (type.equals("boolean") && v instanceof String) {
                if (v.equals("true") || v.equals("false")) this.value = Boolean.valueOf((String) v);
                else this.value = ((CheckBox) input).isChecked();
            }
            else this.value = v;
        }

        public void update() {
            if (input != null) input.setText(this.getValue());
        }

        public String updateValue() {
            if (input != null) this._setValue(input.getText().toString());
            return this.getValue();
        }

        public boolean exists() {
            return !this.value.equals("");
        }

        public void increment() {
            if (type.equals("int")) {
                this.setValue((Integer) this.value +1);
            }
        }

        public void decrement() {
            if (type.equals("int") && !value.equals(0)) {
                this.setValue((Integer) this.value -1);
            }
        }

    }
}

